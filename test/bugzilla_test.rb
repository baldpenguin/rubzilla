#!/usr/bin/env ruby
#require File.dirname(__FILE__) + '/../../lib/bugzilla.rb'
require 'bugzilla'
require 'test/unit'

class BugzillaTest < Test::Unit::TestCase
  def setup
    @b = Bugzilla::Bugzilla.instance
  end

  def test_bugzilla_inst
    assert(@b.is_a?(Bugzilla::Bugzilla))
  end

  def test_bugzilla_ver
    assert(@b.version == '3.0')
  end

  def test_new_valid
    bug = valid_bug
    assert(bug.valid?, bug.errors.inspect)
  end

  def test_new_invalid
    bug = Bugzilla::Bug.new
    assert(!bug.valid?)
  end

  def test_new_id_is_nil
    bug = Bugzilla::Bug.new
    assert_nil(bug.id, @b.defaults.inspect)
  end

  def test_create_missing_fields
    bug = Bugzilla::Bug.create(Bugzilla::Bug.new)
    assert(bug.is_a?(Bugzilla::Bug))
    assert(!bug.errors.empty?, bug.errors.inspect)
  end

  def test_create_valid
    bug = valid_bug
    bug = Bugzilla::Bug.create(bug)
    assert(bug.errors.empty?, bug.errors.inspect)
    assert_not_nil(bug.id)
  end

  def test_create2_invalid
    bug = Bugzilla::Bug.new
    assert_raise(Bugzilla::BugInvalid) { Bugzilla::Bug.create!(bug) }  
  end
  
  def test_create2_valid
    bug = valid_bug
    assert_nothing_raised(bug.errors.inspect) { bug = Bugzilla::Bug.create!(bug) }
    assert_not_nil(bug.id)
  end
  
  def test_find
    bug = @b.find(1)
    assert(bug.is_a?(Bugzilla::Bug))
    assert(bug.creation_time.is_a?(Time))
    assert(bug.id == 1, bug.id.inspect)
    assert(bug.errors.empty?, bug.errors.inspect)
  end

  def test_find_nonexistent
    assert_raise(Bugzilla::BugNotFound) { @b.find(1234) }
  end

  def test_get_attr
    bug = valid_bug
    assert_equal(bug.op_sys, @b.defaults[:op_sys])
    assert(bug[:op_sys])
  end

  def test_set_attr
    bug = valid_bug
    bug.product = 'test1'
    assert_equal(bug.product, 'test1')
  end

  def test_save_invalid
    bug = Bugzilla::Bug.new
    assert(bug.id.nil?)
    assert(!bug.save, bug.errors.inspect)
    assert(!bug.errors.empty?, bug.errors.inspect)
    assert_nil(bug.id)
  end

  def test_save_valid
    bug = valid_bug
    assert_nil(bug.id)
    assert(bug.save, bug.errors.inspect)
    assert(bug.errors.empty?, bug.errors.inspect)
    assert_not_nil(bug.id)
  end

  def test_save2_invalid
    bug = Bugzilla::Bug.new
    assert(bug.id.nil?)
    assert_raise(Bugzilla::BugInvalid) { bug.save! }  
    assert(!bug.errors.empty?, bug.errors.inspect)
    assert_nil(bug.id)
  end

  def test_save2_valid
    bug = valid_bug
    assert(bug.id.nil?)
    assert_nothing_raised(bug.errors.inspect) { bug.save! } 
    assert(bug.errors.empty?, bug.errors.inspect)
    assert_not_nil(bug.id)
  end


  private

  def valid_bug
    Bugzilla::Bug.new(:comment=>'Comment',
                       :component=>'TestComponent',
                       :product=>'TestProduct',
                       :short_desc=>'Short description',
                       :version=>'unspecified')
  end

#  def all_errors
#    @errors = Bugzilla::Errors.new
#    Bugzilla::Bug::REQUIRED_CREATE_FIELDS.each { |b| @errors.add({b => ['is required', nil]}) }
#    @errors
#  end
end
