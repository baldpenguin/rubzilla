#!/usr/bin/env ruby
config_file = File.join(File.dirname(__FILE__),'..','..','..','config','bugzilla.yml')
unless File.exist?(config_file)
  File.open(config_file, 'w') do |f|
    f.write <<EOF
# Configuration for bugzilla
bugzilla:
# Basic configuration
  login: mylogin@example.com
  password: mypassword
  base_url: http://bugzilla.example.com/
# Defaults (product configuration)
  :defaults:
    #:product: TestProduct
    #:component: TestComponent
    #:version: unspecified
    :rep_platform: All
    :op_sys: All
    # Default severity and priority
    :bug_severity: normal
    :priority: P3
EOF
  end
end 
