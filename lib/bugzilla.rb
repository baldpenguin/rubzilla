#!/usr/bin/env ruby
require 'xmlrpc/client'
require 'logger'
require 'singleton'
require 'yaml'

# Bugzilla singleton class is used for posting bug reports to bugzilla
module Bugzilla

  class Bugzilla
    include Singleton

    attr_reader :defaults

    private
    def initialize
      # If we run under Rails, use the default locations, otherwise use the same
      # directory as the lib
      if defined?(RAILS_ROOT)
        log_path = File.join(RAILS_ROOT,'log')
        config_path = File.join(RAILS_ROOT,'config')
      else
        log_path = File.join(File.dirname(__FILE__),'..', 'log')
        config_path = File.join(File.dirname(__FILE__),'..', 'config')
      end

      log_file = File.join(log_path, 'bugzilla.log')
      config_file = File.join(config_path, 'bugzilla.yml') 

      # Create a logger, keep 5 old logs of 1MB each
      @log = Logger.new(log_file,5,1048576)

      config = YAML::load(IO.read(config_file))['bugzilla']
      unless config
        @log.fatal("Cannot load configuration file.")
        exit(-1)
      end

      # Global configuration
      @login = config['login']
      @password = config['password']
      @base_url = config['base_url']
 
      # Flag for Logged in to Bugzilla
      @logged_in = false

      # Default bug report configuration
      @defaults=config[:defaults]

      @xmlrpc_url = @base_url + '/xmlrpc.cgi'
      @server = XMLRPC::Client.new2(@xmlrpc_url)
    end

    public

    def create(bug)
      raise 'Bugzilla::Bug expected' unless bug.is_a?(Bug)
      if bug.valid?
        login
        # Don't pass the 'internals' hash to Bugzilla API, it cannot handle it.
        new_bug = bug.dup
        new_bug.delete(:internals)
        bug.id = @server.call('Bug.create', new_bug)['id']
        return bug
      else
        return false
      end
    rescue XMLRPC::FaultException => e
      @log.error("XMLRPC::FaultException: #{e.faultCode} - #{e.faultString}")
      return false
    end

    def find(*args)
      case arg = args.shift
        when :first then raise 'Unimplemented'
        # Ugly hack - iterate over bugs. Bugzilla doesn't support searching
        # via the XMLRPC api
        when :all then
          bug_id = 1
          collection = []
          while res = getBug(bug_id)
            collection.push(res.first)
            bug_id += 1
          end
          collection
        else
          res = getBug(arg)
          raise BugNotFound unless res
          res.first
      end
    end

    def count(*args)
      find(*args).size
    end

    # Returns the Bugzilla version
    def version
      @version ||= @server.call('Bugzilla.version')['version']
    end

    # Returns a list of products
    def products
      # This action is quite slow and expensive, so we'd rather cache
      # the result.
      @products ||=
      begin
        login
        product_ids = @server.call('Product.get_accessible_products')['ids']
        @server.call('Product.get_products', :ids => product_ids)['products'].collect { |p| Product.new(p) }
      end
    rescue XMLRPC::FaultException => e
      @log.error("XMLRPC::FaultException: #{e.faultCode} - #{e.faultString}")
      nil
    end

    # Finds a product by id
    def getProduct(id)
      products.find { |p| p.id == id }
    end
 
    private

    # Logs into Bugzilla using the credentials specified in the
    # configuration file
    def login
      unless @logged_in
        @server.call('User.login', 
                     {'login'=>@login, 'password'=>@password, 'remember'=>1})
        # Workaround Bugzilla's broken cookies handling
        if @server.cookie =~ /Bugzilla_logincookie=([^;]+)/
          @server.cookie = "Bugzilla_login=1; Bugzilla_logincookie=#{$1}"
          @logged_in = true
        end
      end
    end

    def getBug(bugid)
      @log.info("Getting bug #{bugid}")
      bugid = [bugid] unless bugid.is_a?(Array)
      @server.call('Bug.get_bugs', {'ids' => bugid})['bugs'].collect { |b| Bug.new(b) }
    rescue XMLRPC::FaultException => e
      @log.error("XMLRPC::FaultException: #{e.faultCode} - #{e.faultString}")
      nil
    end
  end

  class Bug < Hash
    REQUIRED_CREATE_FIELDS = [:bug_severity,:comment,:component,:op_sys,:priority,
                              :product,:rep_platform,:short_desc,:version]

    attr_accessor :errors

    def initialize(hash={})
      @errors = Errors.new(self)
      @bugzilla = Bugzilla.instance
      new_hash = @bugzilla.defaults.dup
      # Add items from the provided hash to the new one
      if ! hash.empty?
        hash.each_key {|k| new_hash[k.to_sym]= hash[k]}
      end
      self.merge!(new_hash)
      # Move the required fields to the main hash, because this way they're
      # expected to appear when creating a bug
      self[:internals] ||= {}
      unless self[:internals].empty?
        %w{short_desc version}.each do |key|
          self[key.to_sym] = self[:internals].delete(key)
        end
      end
    end

    def self.find(*args)
      bugzilla = Bugzilla.instance
      bugzilla.find(*args)
    end

    def self.count(*args)
      bugzilla = Bugzilla.instance
      bugzilla.count(*args)
    end

    def self.create(options)
      raise "Bug expected" unless options.respond_to?(:keys)
      bugzilla = Bugzilla.instance
      bug = Bug.new(options) 
      bugzilla.create(bug)
      bug
    end

    def self.create!(options)
      raise "Bug expected" unless options.respond_to?(:keys)
      bugzilla = Bugzilla.instance
      bug = Bug.new(options)
      res = bugzilla.create(bug)
      raise BugInvalid.new(bug) unless bug.errors.empty?
      raise "Cannot create, see log" unless res
      bug
    end

    def save
      @bugzilla.create(self)
    end

    def save!
      res = @bugzilla.create(self)
      raise BugInvalid.new(self) unless self.errors.empty?
      raise "Cannot create, see log #{res.inspect}" unless res
      self
    end

    def to_s
      id.to_s
    end

    # Return the bug id if it exists in bugzilla, i.e. saved or retrieved
    # otherwise, return nil
    def id
      if self[:internals]
        self[:internals]['bug_id']
      else
        nil
      end
    end

    def id=(bug_id)
      self[:internals]['bug_id']=bug_id
    end

    def last_change_time
      self[:last_change_time] ? self[:last_change_time].to_time : nil
    end

    def creation_time
      self[:creation_time] ? self[:creation_time].to_time : nil
    end

    # Internal class needed to fake ActiveRecord's content_columns array
    class BugColumn
      attr_reader :name

      def initialize(name)
        @name = name.to_s
      end

      def human_name
        @name.capitalize
      end
    end

    def self.content_columns
      REQUIRED_CREATE_FIELDS.collect { |c| BugColumn.new(c) }
    end

    def valid?
      self.errors.clear
      missing = Bug::REQUIRED_CREATE_FIELDS - self.keys
      if missing.empty?
        true
      else
        missing.each {|m| self.errors.add(m,'is required')}
        false
      end
    end

    def method_missing(method_id, *args)
      method_id = method_id.id2name.to_sym
      if method_id.to_s =~ /(.*)=$/
        method_id = $1.to_sym
        self[method_id] = args.first
      else
        self[method_id] if self.has_key?(method_id)
      end
    end
  end

  # This class represents the Bugzilla's "products"
  class Product

    # Internal classes

    # A class which represents Products' components
    class Component
      attr_reader :id, :description, :name

      def initialize(hash={})
        @name = hash['name']
        @id = hash['id']
        @description = hash['description']
      end

      def to_s
        @id.to_s
      end
    end

    # A class which represents Products' versions
    class Version
      attr_reader :id, :name

      def initialize(hash={})
        @name = hash['value']
        @id = hash['id']
      end

      def to_s
        @id.to_s
      end
    end

    attr_accessor :id, :name, :description, :components, :versions

    # Constructor for the Products class
    def initialize(hash={})
      @id = hash['id']
      @name = hash['name']
      @description = hash['description']
      @components = hash['components'].collect { |c| Component.new(c) }
      @versions = hash['versions'].collect { |v| Version.new(v) }
    end

    def to_s
      @id.to_s
    end

    # Find a product by id or :all
    def self.find(*args)
      product_id = args.first
      bugzilla = Bugzilla.instance
      if product_id == :all
        bugzilla.products
      else
        raise ArgumentError unless product_id.is_a?(Fixnum)
        raise ProductNotFound unless product = bugzilla.getProduct(product_id)
        product
      end
    end

    def self.count(*args)
      bugzilla = Bugzilla.instance
      bugzilla.products.size
    end
  end


  class Errors
    include Enumerable
    attr_accessor :errors

    @@default_error_messages = {
      :invalid => "is invalid",
      :empty => "can't be empty"
    }

    def initialize(base)
      @base, @errors = base, {}
    end

    def add(attribute, msg = @@default_error_messages[:invalid])
      @errors[attribute.to_sym] = [] if @errors[attribute.to_sym].nil?
      @errors[attribute.to_sym] << msg
    end

    def clear
      @errors = {}
    end

    def empty?
      @errors.empty?
    end

    def each
      @errors.each_key { |attr| @errors[attr].each { |msg| yield attr, msg } }
    end

    def length
      @errors.length
    end

    def on(key)
      errors = @errors[key.to_sym]
      return nil if errors.nil?
      errors.size == 1 ? errors.first : errors
    end
    
    alias :[] :on
    alias :size :length
    alias :count :length
  end

  class BugInvalid < StandardError
    attr_reader :bug
    def initialize(bug)
      @bug = bug
      super
    end
  end

  class BugNotFound < StandardError
  end

  class ProductNotFound < StandardError
  end

end
